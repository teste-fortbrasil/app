import React, { Component } from 'react';
import { Text, View, Image, FlatList, ActivityIndicator, TouchableOpacity, ToastAndroid } from 'react-native';
// import {} from ''
import api from './services/api'
import { SearchBar } from 'react-native-elements';

export default class App extends Component {
  constructor() {
    super()
    this.state = {
      companies: [],
      isLoading: true,
      search: '',
      page: 1
    }
  }
  renderItem = ({ item }) => {
    return (
      <TouchableOpacity style={{ flex: 1, flexDirection: 'row', marginBottom: 5 }}
        onPress={() => ToastAndroid.show(item.name, ToastAndroid.SHORT)}>
        <Image style={{ width: 100, height: 100, margin: 5 }}
          source={{ uri: item.logo }}/>
        <View style={{ flex: 1, justifyContent: 'center', marginLeft: 3 }}>
          <Text style={{ fontSize: 18, color: 'darkgreen', marginBottom: 10 }}>
            {item.name}
          </Text>
          <Text style={{ fontSize: 16 }}>
            {item.address}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }
  renderSeparator = ({item}) => {
    return (
      <View
        style={{ height: 2, width: '100%', backgroundColor: 'gray' }}>
      </View>
    )
  }

  updateSearch = (search) => {
    this.setState({page:1,search},()=>{
        this.listCompanies(false,search);
    })
  }

  listCompanies(concat = false,search="") {
    api.get(`company?page=${this.state.page}&address=${search}`)
      .then(({data}) => {
        this.setState({
          companies: concat ? [...this.state.companies, ...data.response.data] : data.response.data,
          isLoading: false,
          page: this.state.page + 1,
          search
        })
      })
      .catch((error) => console.error(error))
  }

  componentDidMount () {
    this.listCompanies()
  }

  render() {

    return (
      <View style={{flex:1}}>
        <SearchBar
          placeholder="Digite o endereço..."
          onChangeText={this.updateSearch}
          value={this.state.search}
        />
        {this.state.isLoading ?
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <ActivityIndicator size='large' color='#330066' animating />
          </View> :
          <View style={{ flex: 1 }}>
            <FlatList
              // ListHeaderComponent={this.renderHeader}
              data={this.state.companies}
              renderItem={this.renderItem}
              keyExtractor={(item) => item.id}
              ItemSeparatorComponent={this.renderSeparator}
              onEndReachedThreshold={0.1}
              onEndReached={() =>{
                this.listCompanies(true,this.state.search)
              }}
            />
          </View>}
        </View>
    );
  }
};
